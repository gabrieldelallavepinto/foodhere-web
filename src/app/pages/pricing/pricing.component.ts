import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss'],
})
export class PricingComponent implements OnInit {
  billing = 0.0;
  percentageBilling = 0.01;
  stores = 1;

  finalPrice = 0.0;
  maxPriceStore = 60.00;
  maxInvoicingStore = 12000.00;

  percentageBillingGlovo = 0.15;
  finalPriceGlovo = 0.0;

  cards = [
    {
      icon: 'fas fa-solar-panel',
      color: '#b62222',
      title: 'PANEL DE ADMINISTRACIÓN',
      text: 'Gestiona toda tu empresa y productos desde un único panel.',
    },
    {
      icon: 'fas fa-mobile-alt',
      color: '#00ac69',
      title: 'APP PERSONALIZADA PARA TU NEGOCIO',
      text: 'Aplicación para clientes nativa en Android e IOS con diseño exclusivo.',
    },
    {
      icon: 'fas fa-user',
      color: '#9C27B0',
      title: 'FIDELIZACIÓN DE CLIENTES',
      text: 'Ten a tus clientes contentos con el sistema de fidelización.',
    },
    {
      icon: 'fas fa-file-alt',
      color: '#FBC02D',
      title: 'CARTA DIGITAL',
      text: 'Muestra tu carta a trasés de tu app o de la web mediante un código qr.',
    },
    {
      icon: 'fas fa-box-open',
      color: '#007bff',
      title: 'GESTOR Y TRACKING DE PEDIDOS',
      text: 'Sigue en todo momento la gestión de tus pedidos.',
    },
    {
      icon: 'fas fa-desktop',
      color: '#fd9260',
      title: 'PANTALLA DE COCINA',
      text: 'Gestiona la entrada de los pedidos directamente en la cocina de tu negocio.',
    },

  ];

  constructor() {}

  ngOnInit(): void {}

  /** Calcula el precio mensual dependiendo de los sliders */
  calculatePrice(): void {
    this.finalPrice = this.billing * this.percentageBilling;

    if (this.finalPrice > this.maxPriceStore) {
      this.finalPrice = this.maxPriceStore;
    }

    this.finalPrice = this.finalPrice * this.stores;
    this.finalPriceGlovo = this.billing * this.percentageBillingGlovo * this.stores;


  }
}
