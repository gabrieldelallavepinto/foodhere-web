import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricingComponent } from './pricing.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [PricingComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  exports: [PricingComponent]
})
export class PricingModule { }
