import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  navbarTop = true;

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('window:scroll', ['$event'])
  onScroll($event): void{
    console.log('scrolling...');
    console.log(window.scrollY);

    if (window.scrollY === 0) {
      this.navbarTop = true;
    } else {
      this.navbarTop = false;
    }
  }

}
